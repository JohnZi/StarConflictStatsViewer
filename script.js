function search() {
	var names = [];
	var inputs = document.getElementsByTagName("input");
	for(let i = 0; i < inputs.length; i++) {
		inputs[i].value !== "" && names.push(inputs[i].value);
	}
	var responses = names.map(name => fetch("https://gmt.star-conflict.com/pubapi/v1/userinfo.php?nickname=" + name));
	Promise.all(responses)
		.then( r => Promise.all(r.map(p => p.json())))
		.then( j => fillStats(j));
}

function addInput() {
	var el = document.getElementsByTagName("input");
	var i = el.length;
	var input = document.createElement("input");
	input.id = "name" + i;
	input.size = 9;
	input.type = "text";
	input.placeholder = "Name";
	input.onkeyup = function(){if(event.keyCode===13){search();}};
	el[i-1].after(input);
}

function removeInput() {
	var el = document.getElementsByTagName("input");
	(el.length > 1) && el[el.length-1].remove();
}

function fillStats(json) {
	var length = json.length;
	var bool = true;
	var section;
	var table;
	for(let i = 0; i < length; i++) {
		if (json[i].result !== "ok") {
			if (json[i].result === "error") {
				document.getElementById('msg').innerHTML = "<p>"+ json[i].text + "</p>";
			} else {
				document.getElementById('msg').innerHTML = "<p>unknown error</p>";
			}
			bool = false;
			break;
		}
		document.getElementById('msg').innerHTML = "";

	}
	if (bool) {
		var row;
		table = document.createElement('table');
		section = document.getElementById('stats');
		table.insertStatsRow("Name", json.map( j => j.data.nickName ), false);
		table.insertStatsRow("Pilot rating", json.map( j => j.data.effRating ? Math.round(j.data.effRating) : 500));
		table.insertStatsRow("Pilot karma", json.map( j => j.data.openWorld && j.data.openWorld.karma ? j.data.openWorld.karma : 0));
		table.insertStatsRow("Fleet strength", json.map( j => j.data.prestigeBonus ? Math.round(j.data.prestigeBonus * 100) : 0));
		table.insertStatsRow("Clearance level", json.map( j => j.data.accountRank ? j.data.accountRank : 0));
		table.insertStatsRow("Corporation", json.map( j => j.data.clan ? "[" + j.data.clan.tag  + "] " + j.data.clan.name: "none"), false);
		table.insertStatsRow("PvP rating", json.map( j => j.data.clan ? j.data.clan.pvpRating : 0));
		table.insertStatsRow("PvE rating", json.map( j => j.data.clan ? j.data.clan.pveRating : 0));
		row = table.insertRow();
		createHeader(row, "PvP Stats", length + 1);
		table.insertStatsRow("Total PvP battles", json.map( j => j.data.pvp ? j.data.pvp.gamePlayed : 0));
		table.insertStatsRow("Total PvP wins", json.map( j => j.data.pvp ? j.data.pvp.gameWin : 0));
		table.insertStatsRow("Win/Loss ratio", json.map( j => (j.data.pvp ? j.data.pvp.gameWin / (j.data.pvp.gamePlayed - j.data.pvp.gameWin) : 0)));
		table.insertStatsRow("Total ships destroyed", json.map( j => j.data.pvp ? j.data.pvp.totalKill : 0));
		table.insertStatsRow("Total assists", json.map( j => j.data.pvp ? j.data.pvp.totalAssists : 0));
		table.insertStatsRow("Lost ships", json.map( j => j.data.pvp ? j.data.pvp.totalDeath : 0), true, true);
		table.insertStatsRow("K/D", json.map( j => j.data.pvp ? j.data.pvp.totalKill/j.data.pvp.totalDeath : 0));
		table.insertStatsRow("KDA", json.map( j => j.data.pvp ? (j.data.pvp.totalKill+j.data.pvp.totalAssists)/j.data.pvp.totalDeath : 0));
		table.insertStatsRow("Total damage dealt", json.map( j => j.data.pvp ? Math.round(j.data.pvp.totalDmgDone) : 0));
		table.insertStatsRow("Damage per second", json.map( j => j.data.pvp ? (j.data.pvp.totalDmgDone/j.data.pvp.totalBattleTime * 1000) : 0));
		table.insertStatsRow("Repairs, including allies", json.map( j => j.data.pvp ? j.data.pvp.totalHealingDone : 0));
		table.insertStatsRow("Total capture points", json.map( j => j.data.pvp ? j.data.pvp.totalVpDmgDone : 0));
		table.insertStatsRow("Time in battle", json.map( j => {
			if(j.data.pvp) {
				var daysInBattle = Math.floor(j.data.pvp.totalBattleTime / 3600000 / 24);
				var hoursInBattle = Math.floor(j.data.pvp.totalBattleTime / 3600000 - daysInBattle * 24);
				var minutesInBattle = Math.floor((j.data.pvp.totalBattleTime / 60000 - daysInBattle * 60 * 24 - hoursInBattle * 60));
				return daysInBattle + "d " + hoursInBattle + "h " + minutesInBattle + "min";
		} else { return 0}}), false);
		row = table.insertRow();
		createHeader(row, "Average per Battle", length + 1);
		table.insertStatsRow("Average kills in battle", json.map( j => j.data.pvp ? j.data.pvp.totalKill / j.data.pvp.gamePlayed : 0));
		table.insertStatsRow("Average assists in battle", json.map( j => j.data.pvp ? j.data.pvp.totalAssists / j.data.pvp.gamePlayed : 0));
		table.insertStatsRow("Average Deaths", json.map( j => j.data.pvp ? j.data.pvp.totalDeath / j.data.pvp.gamePlayed : 0), true, true);
		table.insertStatsRow("Average Damage", json.map( j => j.data.pvp ? j.data.pvp.totalDmgDone / j.data.pvp.gamePlayed : 0));
		table.insertStatsRow("Average Healing", json.map( j => j.data.pvp ? j.data.pvp.totalHealingDone / j.data.pvp.gamePlayed : 0));
		row = table.insertRow();
		createHeader(row, "PvE Stats", length + 1);
		table.insertStatsRow("Total PvE battles", json.map( j => j.data.pve ? j.data.pve.gamePlayed : 0));
		table.insertStatsRow("Power lv.", json.map( j => j.data.pve && j.data.pve.unlimPve_playerAttackLevel ? j.data.pve.unlimPve_playerAttackLevel : 0));
		table.insertStatsRow("Defence lv.", json.map( j => j.data.pve && j.data.pve.unlimPve_playerDefenceLevel ? j.data.pve.unlimPve_playerDefenceLevel : 0));
		table.insertStatsRow("\'Blackwood\' Shipyard", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.bigship_building_easy ? j.data.pve.unlimPve_missionLevels.bigship_building_easy : 0));
		table.insertStatsRow("Defence contract", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.nalnifan_lumen_waves_T1 ? j.data.pve.unlimPve_missionLevels.nalnifan_lumen_waves_T1 : 0));
		table.insertStatsRow("Fire support", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.planet_war_waves_T1 ? j.data.pve.unlimPve_missionLevels.planet_war_waves_T1 : 0));
		table.insertStatsRow("Operation \'Ice Belt\'", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.asteroid_building_t1 ? j.data.pve.unlimPve_missionLevels.asteroid_building_t1 : 0));
		table.insertStatsRow("The price of trust", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.pve_empfrontier_waves_T1 ? j.data.pve.unlimPve_missionLevels.pve_empfrontier_waves_T1 : 0));
		table.insertStatsRow("Processing rig", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.loot_geostation_normal ? j.data.pve.unlimPve_missionLevels.loot_geostation_normal : 0));
		table.insertStatsRow("Operation \'Monolith\'", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.pve_desttown_waves_easy ? j.data.pve.unlimPve_missionLevels.pve_desttown_waves_easy : 0));
		table.insertStatsRow("Captured Dreadnought", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.capture_repairbase_t1 ? j.data.pve.unlimPve_missionLevels.capture_repairbase_t1 : 0));
		table.insertStatsRow("\'Ellydium\' plant raid", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.pve_frozen_station_t2 ? j.data.pve.unlimPve_missionLevels.pve_frozen_station_t2 : 0));
		table.insertStatsRow("Operation \'Crimson Haze\'", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.pve_jericho_base_t2 ? j.data.pve.unlimPve_missionLevels.pve_jericho_base_t2 : 0));
		table.insertStatsRow("Ariadne\'s Thread", json.map( j => j.data.pve && j.data.pve.unlimPve_missionLevels && j.data.pve.unlimPve_missionLevels.bigship_building_2_easy ? j.data.pve.unlimPve_missionLevels.bigship_building_2_easy : 0));
		table.insertStatsRow("Temple of Last Hope", json.map ( j => j.data.pve && j.data.pve.wavePve_maxWave ? j.data.pve.wavePve_maxWave : 0));
		row = table.insertRow();
		createHeader(row, "Co-op vs. AI", length + 1);
		table.insertStatsRow("Total Co-op vs. AI battles", json.map( j => j.data.coop ? j.data.coop.gamePlayed : 0));
		table.insertStatsRow("Total Co-op vs. AI wins", json.map( j => j.data.coop ? j.data.coop.gameWin : 0));
		table.insertStatsRow("Win/Loss ratio", json.map( j => (j.data.coop ? j.data.coop.gameWin / (j.data.coop.gamePlayed - j.data.coop.gameWin) : 0)));
		table.insertStatsRow("Time in battle", json.map( j => {
			if(j.data.coop) {
				var daysInBattle = Math.floor(j.data.coop.totalBattleTime / 3600000 / 24);
				var hoursInBattle = Math.floor(j.data.coop.totalBattleTime / 3600000 - daysInBattle * 24);
				var minutesInBattle = Math.floor((j.data.coop.totalBattleTime / 60000 - daysInBattle * 60 * 24 - hoursInBattle * 60));
				return daysInBattle + "d " + hoursInBattle + "h " + minutesInBattle + "min";
		} else { return 0}}), false);
		section.childNodes.forEach(function (item, index, array) {
			section.removeChild(item);
		});
		section.appendChild(table);
	}
}

HTMLTableElement.prototype.insertStatsRow = function (name, values, color = true, invert = false) {
	var i;
	var row = this.insertRow();
	createHeader(row, name);
	color = values.length == 1 ? false : color;
	for(i = 0; i < values.length; i++) {
		var cell = row.insertCell();
		if(typeof values[i] === 'number' && !Number.isInteger(values[i])) {
			cell.innerHTML = values[i].toFixed(2);
		} else {
			cell.innerHTML = values[i];
		}
		color && setColor(Math.min(...values), Math.max(...values), values[i], cell, invert);
	}

}

function setColor(min, max, value, cell, invert = false) {
	if (max === value) {
		cell.className = invert ? "red" : "green";
	} else if (min === value) {
		cell.className = invert ? "green" : "red";
	}
}

function createHeader(row, text, colspan = 1) {
	var header = document.createElement("TH");
	header.innerHTML = text;
	header.scope = "row";
	header.colSpan = colspan;
	row.appendChild(header);
}

