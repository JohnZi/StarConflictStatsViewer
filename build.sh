#!/usr/bin/env sh

unpacked_dir="/media/StarConflict/unpackedFiles-${SC_VERSION}/strings"

patterns="$(grep -o '@[^@]*@' script.js.in)"
patternsD="$(grep -o '\$[^\$]*\$' script.js.in)"

for i in "${unpacked_dir}"/*
do
	lang=$(basename "$i")
	if [ "$lang" = 'english' ] || [ "$lang" = 'russian' ]
	then
		mkdir -p "$lang"
		cp style.css "${lang}/style.css"
		cp index.html "${lang}/index.html"
		cp script.js.in "${lang}/script.js"
		for p in ${patterns}
		do
			p=$(echo ${p} | sed 's/@//g')
			string=$(grep -Po "\"${p}\"\t\"\K[^\&]*(?=\")" "${i}/string.txt" || echo undefined)
			string="$(echo $string | sed  -e "s/'/\\\\\\\'/g" -e 's/ \$val\$//g' -e 's/:$//g')"
			sed -e "s%@${p}@%${string}%g" -i "${lang}/script.js"
		done
		if [ -f ${lang}.lang ]
		then
		for p in ${patternsD}
			do
				p=$(echo ${p} | sed 's/\$//g')
				string=$(grep -Po "\"${p}\"\t\"\K[^\&]*(?=\")" "./${lang}.lang" || echo undefined)
				string="$(echo $string | sed  -e "s/'/\\\\\\\'/g" -e 's/ \$val\$//g' -e 's/:$//g')"
				sed -e "s/\\\$${p}\\\$/${string}/g" -i "${lang}/script.js"
			done
		fi
		[ "$lang" = 'english' ] && sed -e 's/Themple/Temple/g' -i "$lang/script.js"
		[ "$lang" = 'russian' ] && sed -e 's/lang=en/lang=ru/g' -i "$lang/index.html"
	fi
done

[ -f english/script.js ] && cp english/script.js script.js
